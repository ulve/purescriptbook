module Test.Main where

import Prelude
import Data.AdressBook as AddressBook
import Data.AdressBook (AddressBook, emptyBook, findEntry, insertEntry)
import Data.Maybe (isJust, isNothing)
import Test.Unit (test, suite)
import Test.Unit.Assert (assert)
import Test.Unit.Main (runTest)

main = runTest do
    let address = { street: "street", city: "city", pnr: "pnr" }
    let entry = { firstName: "first", lastName: "last", address: address }
    let nonEmptyBook = AddressBook.insertEntry entry AddressBook.emptyBook
    suite "Filter" do
        test "En tom adressbok returnerar Nothing" do
            assert "Borde vara Nothing" $ isNothing $ AddressBook.findEntry "kisse" "misse" AddressBook.emptyBook  
        test "Det går att hitta med för och efternamn" do
            assert "Borde hitta en person" $ isJust $ AddressBook.findEntry "first" "last" nonEmptyBook
        test "Borde vara Nothing" do
            assert "Borde vara Nothing" $ isNothing $ AddressBook.findEntry "first" "misse" nonEmptyBook
        test "Man får fortfarande ett svar om det är flera" do
            let twoEntries = AddressBook.insertEntry entry nonEmptyBook
            assert "Borde" $ isJust $ AddressBook.findEntry "first" "last" twoEntries