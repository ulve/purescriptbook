module Data.AdressBook where
  
import Prelude
import Control.Plus (empty)
import Data.List (List(..), filter, head)
import Data.Maybe (Maybe)

type Entry = 
    { firstName :: String
    , lastName :: String
    , address :: Address
    }

type Address = 
    { street :: String
    , city :: String
    , pnr :: String
    }

type AddressBook = List Entry

showAddress :: Address -> String
showAddress addr = addr.street <> ", " <> 
            addr.city <> ", " <> 
            addr.pnr

showEntry :: Entry -> String
showEntry entry = entry.lastName <> ", " <> 
          entry.firstName <> ": " <> 
          showAddress entry.address

emptyBook :: AddressBook
emptyBook = empty -- Empty list from Data.List

insertEntry :: Entry -> AddressBook -> AddressBook
insertEntry entry book = Cons entry book 
{- Går att skriva pointfree som  insertEntry = Cons om man vill det:
   insertEntry entry book = (Cons entry) book
   insertEntry entry = Cons entry
   insertEntry = Cons
-}

findEntry :: String -> String -> AddressBook -> Maybe Entry
findEntry firstName lastName book = head $ filter filterEntry book
    where
      filterEntry :: Entry -> Boolean
      filterEntry entry = entry.firstName == firstName && 
                          entry.lastName == lastName
{- Här kan man också göra lite tacit programmering book är fri 
   så det går helt enkelt att ta bort book
   $ är bara apply och används framför allt för att ta bort 
   parenteser eftersom den är högseassociativ.
   head $ filter filterEntry book ==
   head (filter filterEntry book)
   kollar man på det så är book inte möjlig att ta bort för 
   den är inte fri längst till höger
   men man kan använda kompositionsoperatorn för att bygga samman
   head och filter
   (head <<< filter filterEntry) book
   då är helt plötsligt book ledig längst till höger och går att 
   ta bort eftersom den är implicit!

   findEntry first last = head <<< filter filterEntry
-}