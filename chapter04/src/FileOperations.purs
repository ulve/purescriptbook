module FileOperations where

import Data.Path
import Data.Array
import Prelude
import Data.Foldable
import Control.MonadPlus
import Data.Maybe

allFiles :: Path -> Array Path
allFiles path = path : concatMap allFiles (ls path)

allFiles' :: Path -> Array Path
allFiles' path = path : do
    child <- ls path
    allFiles' child

onlyFiles :: Path -> Array Path
onlyFiles = filter (not isDirectory) <<< allFiles 

find :: (Maybe Int -> Maybe Int -> Boolean) -> Path -> Maybe Path
find cmp root = 
    foldl c Nothing $ (onlyFiles root)
    where
        c (Just x) y = if cmp (size x) (size y) then Just x else Just y
        c Nothing y = Just y

findLargest :: Path -> Maybe Path
findLargest = find (>)

findSmallest :: Path -> Maybe Path
findSmallest = find (<)

whereIs :: String -> Path -> Maybe Path
whereIs q r = head $ allFiles r
  >>= \path  -> ls path
  >>= \child -> guard (filename child == q)
  >>= \_     -> pure path

whereIs' :: String -> Path -> Maybe Path
whereIs' q r = head do
    path <- allFiles r
    child <- ls path
    guard $ filename child == q
    pure path